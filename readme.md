# CCM-Flyers


### Follow these steps
- For each flyer, there should be at least 2 files:  Image and .psd  (If there's any extra related file, push that as well)
- Prefix the name with "Flyer-" followed by numeric month and date. Example for father's day the names should be Flyer-0621-Fathers-Day.png and Flyer-0621-Fathers-Day.psd
